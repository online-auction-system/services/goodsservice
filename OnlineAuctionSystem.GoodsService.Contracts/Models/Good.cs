﻿using System;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace OnlineAuctionSystem.GoodsService.Contracts.Models
{
    public class Good
    {
        [JsonProperty("good_id")] 
        public Guid GoodId { get; set; }

        [JsonProperty("price")] 
        public decimal Price { get; set; }

        [JsonProperty("picture")] 
        public string Picture { get; set; }

        [JsonProperty("description")] 
        public string Description { get; set; }

        [JsonProperty("auction_id")] 
        public Guid? AuctionId { get; set; }

        [JsonProperty("previous_owner_id")]
        public int? PreviousOwnerId { get; set; }

        [JsonProperty("current_owner_id")] 
        public int CurrentOwnerId { get; set; }

        [JsonProperty("name")] 
        public string Name { get; set; }

        [JsonProperty("last_transfer_date")]
        public DateTime? LastTransferDate { get; set; }
    }
}