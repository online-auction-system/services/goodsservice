﻿using System;
using Newtonsoft.Json;

namespace OnlineAuctionSystem.GoodsService.Contracts.Models
{
    public class Auction
    {
        [JsonProperty("auction_id")]
        public Guid AuctionId { get; set; }
        [JsonProperty("holder_id")]
        public int HolderId { get; set; }
        [JsonProperty("start_time")]
        public DateTime StartTime { get; set; }
        [JsonProperty("end_time")]
        public DateTime? EndTime { get; set; }
        [JsonProperty("min_buyer_reputation")]
        public int? MinBuyerReputation { get; set; }
        [JsonProperty("min_seller_reputation")]
        public int? MinSellerReputation { get; set; }
        [JsonProperty("max_goods")]
        public int? MaxGoods { get; set; }
        [JsonProperty("bargain_time")]
        public int? BargainTime { get; set; }
        [JsonProperty("max_participants")]
        public int? MaxParticipants { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("allow_anonymous")]
        public bool? AllowAnonymous  { get; set; }
    }
}