﻿using System;
using System.Threading.Tasks;
using OnlineAuctionSystem.GoodsService.Contracts.Models;

namespace OnlineAuctionSystem.GoodsService.Contracts.Interfaces
{
    public interface IAuctionsClient
    {
        Task<Auction> GetUserAuctionByIdAsync(int userId, Guid auctionId);
    }
}