﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineAuctionSystem.GoodsService.Contracts.Models;

namespace OnlineAuctionSystem.GoodsService.Contracts.Interfaces
{
    public interface IGoodsDatabase
    {
        Task<Good> GetUserGoodAsync(int userId, Guid goodId);
        Task<Good> InsertGoodAsync(Good good, int? maxGoodsOnAuction);
        Task<Good> EditGoodAsync(Good good, int? maxGoodsOnAuction);
        Task<List<Good>> GetAllGoodsAsync(int userId);
        Task<List<Good>> GetGoodsOnAuctionInternalAsync(Guid auctionId);
        Task<bool> DeleteGoodAsync(int userId, Guid goodId);
        Task<bool> RemoveGoodAsync(Guid goodId);
        Task<Good> GetGoodByIdInternalAsync(Guid goodId);
    }
}