﻿using System;
using OnlineAuctionSystem.GoodsService.Contracts.Models;

namespace OnlineAuctionSystem.GoodsService.Contracts.Interfaces
{
    public interface IGoodsOnAuctionsSender
    {
        void SendInfoAboutNewGoodOnAuction(Good good, Auction auction);
    }
}