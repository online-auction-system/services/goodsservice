﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineAuctionSystem.GoodsService.Contracts.Models;

namespace OnlineAuctionSystem.GoodsService.Contracts.Interfaces
{
    public interface IGoodsRepository
    {
        Task<Good> InsertGoodAsync(int ownerId, Good good);
        Task<Good> GetUserGoodAsync(int ownerId, Guid goodId);
        Task<bool> DeleteGoodAsync(int ownerId, Guid goodId);
        Task<bool> RemoveGoodAsync(int auctionOwnerId, Guid goodId);
        Task<Good> EditUserGoodAsync(int userId, Good good, Guid goodId);
        Task<List<Good>> GetAllGoodsAsync(int ownerId);
        Task<List<Good>> GetGoodsOnAuctionInternalAsync(Guid auctionId);
    }
}