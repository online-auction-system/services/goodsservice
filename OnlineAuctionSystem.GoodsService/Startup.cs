using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using OnlineAuctionSystem.Authentication.MiddleWare;
using OnlineAuctionSystem.DataAccessLibrary;
using OnlineAuctionSystem.ExceptionHandlerMiddleware;
using OnlineAuctionSystem.GoodsService.BusinessLogic.Repositories;
using OnlineAuctionSystem.GoodsService.Contracts.Interfaces;
using OnlineAuctionSystem.GoodsService.DataAccess;
using OnlineAuctionSystem.GoodsService.DataAccess.Clients;
using OnlineAuctionSystem.GoodsService.DataAccess.Databases;
using OnlineAuctionSystem.GoodsService.DataAccess.Rmq;

namespace OnlineAuctionSystem.GoodsService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new SnakeCaseContractResolver();
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
            services.AddApiSecretAuth();
            services.AddSingleton<BaseDataAccess>();
            services.AddSingleton<IGoodsRepository, GoodsRepository>();
            services.AddSingleton<IGoodsDatabase, GoodsDatabase>();
            services.AddSingleton<IDatabaseConfig, DbConfig>();
            services.AddSingleton<IAuctionsClient, AuctionsClient>();
            services.AddSingleton<IGoodsOnAuctionsSender, GoodsOnAuctionsSender>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseExceptionHandlerMiddleware();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}