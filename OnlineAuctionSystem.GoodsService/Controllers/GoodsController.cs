﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineAuctionSystem.Entities.Exceptions;
using OnlineAuctionSystem.GoodsService.Contracts.Interfaces;
using OnlineAuctionSystem.GoodsService.Contracts.Models;

namespace OnlineAuctionSystem.GoodsService.Controllers
{
    [Route("[controller]")]
    [Authorize]
    public class GoodsController : BaseController
    {
        private readonly IGoodsRepository _goodsRepository;

        public GoodsController(IGoodsRepository goodsRepository)
        {
            _goodsRepository = goodsRepository;
        }

        [HttpPost("new")]
        public async Task<JsonResult> AddNewGood([FromBody] Good good)
        {
            if (good == null)
            {
                var errorFields = GetModelStateErrorFields<Good>();
                var error = new DeserializationException(errorFields);
                return error.ToJson();
            }

            return new JsonResult(await _goodsRepository.InsertGoodAsync(CurrentUser.UserId, good));
        }

        [HttpDelete("{goodId:guid}")]
        public async Task<JsonResult> DeleteGood(Guid goodId)
        {
            var result = await _goodsRepository.DeleteGoodAsync(CurrentUser.UserId, goodId);
            return new JsonResult(result ? Ok() : new StatusCodeResult(404));
        }

        [HttpPut("{goodId:guid}/remove")]
        public async Task<JsonResult> RemoveGood(Guid goodId)
        {
            var result = await _goodsRepository.RemoveGoodAsync(CurrentUser.UserId, goodId);
            return new JsonResult(result ? Ok() : new StatusCodeResult(500));
        }
        
        [HttpGet("{goodId:guid}")]
        public async Task<JsonResult> GetUserGood(Guid goodId)
        {
            var good = await _goodsRepository.GetUserGoodAsync(CurrentUser.UserId, goodId);
            return good == null ? new JsonResult(new StatusCodeResult(404)) : new JsonResult(good);
        }

        [HttpPost("{goodId:guid}/edit")]
        public async Task<JsonResult> EditGood([FromBody] Good newGood, Guid goodId)
        {
            if (newGood == null)
            {
                var errorFields = GetModelStateErrorFields<Good>();
                var error = new DeserializationException(errorFields);
                return error.ToJson();
            }

            var editedGood = await _goodsRepository.EditUserGoodAsync(CurrentUser.UserId, newGood, goodId);
            return editedGood == null ? new JsonResult(new StatusCodeResult(404)) : new JsonResult(editedGood);
        }

        [HttpGet]
        public async Task<JsonResult> GetAllUserGoods()
        {
            return new JsonResult(await _goodsRepository.GetAllGoodsAsync(CurrentUser.UserId));
        }
        
        [AllowAnonymous]
        [HttpGet("/internal/goods/on-auction/{auctionId:guid}")]
        public async Task<JsonResult> GetGoodsOnAuctionInternal(Guid auctionId)
        {
            return new JsonResult(await _goodsRepository.GetGoodsOnAuctionInternalAsync(auctionId));
        }
    }
}