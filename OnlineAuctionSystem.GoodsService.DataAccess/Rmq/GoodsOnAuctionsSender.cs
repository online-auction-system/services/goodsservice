﻿using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OnlineAuctionSystem.GoodsService.Contracts.Interfaces;
using OnlineAuctionSystem.GoodsService.Contracts.Models;
using OnlineAuctionSystem.GoodsService.DataAccess.Rmq.Models;
using RabbitMQ.Client;

namespace OnlineAuctionSystem.GoodsService.DataAccess.Rmq
{
    public class GoodsOnAuctionsSender : BaseRmqClient, IGoodsOnAuctionsSender
    {
        private static readonly JsonSerializer SnakeSerializer = JsonSerializer.CreateDefault(new JsonSerializerSettings
        {
            ContractResolver = new DefaultContractResolver
            {
                NamingStrategy = new SnakeCaseNamingStrategy
                    {ProcessDictionaryKeys = true, OverrideSpecifiedNames = true}
            }
        });

        public GoodsOnAuctionsSender()
        {
            Channel.ExchangeDeclare("oas.auctions", ExchangeType.Topic, autoDelete: true);
        }

        public void SendInfoAboutNewGoodOnAuction(Good good, Auction auction)
        {
            byte[] body;
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    using (var jsonTextWriter = new JsonTextWriter(streamWriter))
                    {
                        SnakeSerializer.Serialize(jsonTextWriter,
                            new NewGoodOnAuctionRmqModel
                            {
                                AuctionId = auction.AuctionId, AuctionName = auction.Name,
                                AuctionHolderId = auction.HolderId, GoodId = good.GoodId, GoodName = good.Name,
                                GoodOwnerId = good.CurrentOwnerId, PostTime = DateTime.Now
                            });
                        streamWriter.Flush();
                        body = memoryStream.ToArray();
                    }
                }
            }

            Channel.BasicPublish("oas.auctions", "new_good", body: body);
        }
    }
}