﻿namespace OnlineAuctionSystem.GoodsService.DataAccess
{
    public static class StoredProcedures
    {
        public const string InsertGood = "sp_Good_Insert_v0";
        public const string EditGood = "sp_Good_Edit_v0";
        public const string GetAllGoods = "sp_Goods_Get_v0";
        public const string GetGoodsOnAuction = "sp_Goods_Get_By_Auction_v0";
        public const string GetUserGood = "sp_Good_Get_With_Permissions_v0";
        public const string DeleteGoodById = "sp_Good_Delete_v0";
        public const string RemoveGood = "sp_Good_Remove_v0";
        public const string GetGoodById = "sp_Good_Get_v0";
    }
}