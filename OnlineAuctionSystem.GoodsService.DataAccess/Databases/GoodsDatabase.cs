﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using OnlineAuctionSystem.DataAccessLibrary;
using OnlineAuctionSystem.GoodsService.Contracts.Interfaces;
using OnlineAuctionSystem.GoodsService.Contracts.Models;

namespace OnlineAuctionSystem.GoodsService.DataAccess.Databases
{
    public class GoodsDatabase : IGoodsDatabase
    {
        private readonly BaseDataAccess _dataAccess;

        public GoodsDatabase(BaseDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public Task<Good> GetUserGoodAsync(int userId, Guid goodId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@UserId", userId);
            parameters.Add("@GoodId", goodId);
            return _dataAccess.ExecuteStoredProcedureWithSingleResultAsync<Good>(StoredProcedures.GetUserGood,
                parameters);
        }

        public Task<Good> InsertGoodAsync(Good good, int? maxGoodsOnAuction)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Price", good.Price);
            parameters.Add("@Picture", good.Picture);
            parameters.Add("@Description", good.Description);
            parameters.Add("@AuctionId", good.AuctionId);
            parameters.Add("@PreviousOwnerId", good.PreviousOwnerId);
            parameters.Add("@CurrentOwnerId", good.CurrentOwnerId);
            parameters.Add("@Name", good.Name);
            parameters.Add("@LastTransferDate", good.LastTransferDate);
            parameters.Add("@MaxGoodsOnAuction", maxGoodsOnAuction);
            return _dataAccess.ExecuteStoredProcedureWithSingleResultAsync<Good>(StoredProcedures.InsertGood,
                parameters);
        }

        public Task<Good> EditGoodAsync(Good good, int? maxGoodsOnAuction)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@GoodId", good.GoodId);
            parameters.Add("@Price", good.Price);
            parameters.Add("@Picture", good.Picture);
            parameters.Add("@Description", good.Description);
            parameters.Add("@AuctionId", good.AuctionId);
            parameters.Add("@CurrentOwnerId", good.CurrentOwnerId);
            parameters.Add("@Name", good.Name);
            parameters.Add("@MaxGoodsOnAuction", maxGoodsOnAuction);
            return _dataAccess.ExecuteStoredProcedureWithSingleResultAsync<Good>(StoredProcedures.EditGood,
                parameters);
        }

        public async Task<List<Good>> GetAllGoodsAsync(int userId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@OwnerId", userId);
            return (await _dataAccess.ExecuteStoredProcedureWithCollectionResultAsync<Good>(StoredProcedures.GetAllGoods,
                parameters)).ToList();
        }

        public async Task<List<Good>> GetGoodsOnAuctionInternalAsync(Guid auctionId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AuctionId", auctionId);
            return (await _dataAccess.ExecuteStoredProcedureWithCollectionResultAsync<Good>(StoredProcedures.GetGoodsOnAuction,
                parameters)).ToList();
        }

        public Task<bool> DeleteGoodAsync(int userId, Guid goodId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@GoodId", goodId);
            parameters.Add("@UserId", userId);
            return _dataAccess.ExecuteStoredProcedureWithSingleResultAsync<bool>(StoredProcedures.DeleteGoodById,
                parameters);
        }

        public Task<bool> RemoveGoodAsync(Guid goodId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@GoodId", goodId);
            return _dataAccess.ExecuteStoredProcedureWithSingleResultAsync<bool>(StoredProcedures.RemoveGood,
                parameters);
        }

        public Task<Good> GetGoodByIdInternalAsync(Guid goodId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@GoodId", goodId);
            return _dataAccess.ExecuteStoredProcedureWithSingleResultAsync<Good>(StoredProcedures.GetGoodById,
                parameters);
        }
    }
}