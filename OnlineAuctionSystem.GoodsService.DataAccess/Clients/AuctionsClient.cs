﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using OnlineAuctionSystem.GoodsService.Contracts.Interfaces;
using OnlineAuctionSystem.GoodsService.Contracts.Models;
using OnlineAuctionSystem.HttpLibrary;
using OnlineAuctionSystem.HttpLibrary.ContractResolvers;

namespace OnlineAuctionSystem.GoodsService.DataAccess.Clients
{
    public class AuctionsClient : BaseHttpClient, IAuctionsClient
    {
        private readonly TimeSpan _defaultTimeOut;

        public AuctionsClient(ILogger<AuctionsClient> logger)
            : base(logger)
        {
            CreateHttpClient();
            _defaultTimeOut = TimeSpan.FromMinutes(5);
        }

        protected override string ServiceName => "Auctions";
        protected override Entities.Services ServiceId => Entities.Services.Auctions;
        protected override string BaseUrl => "http://localhost:6636";

        public Task<Auction> GetUserAuctionByIdAsync(int userId, Guid auctionId)
        {
            var url = $"/auctions/{auctionId}";
            var reqMsg = new HttpRequestMessage(HttpMethod.Get, url);
            reqMsg.Headers.Add("X-Authenticated-Userid", userId.ToString());
            return SendAsync<Auction, SnakeCaseContractResolver>(reqMsg, _defaultTimeOut);
        }
    }
}