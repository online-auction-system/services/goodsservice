﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation.Results;
using OnlineAuctionSystem.Entities.Exceptions;
using OnlineAuctionSystem.GoodsService.Contracts.Interfaces;
using OnlineAuctionSystem.GoodsService.Contracts.Models;

namespace OnlineAuctionSystem.GoodsService.BusinessLogic.Repositories
{
    public class GoodsRepository : IGoodsRepository
    {
        private readonly IGoodsOnAuctionsSender _rmqSender;
        private readonly IGoodsDatabase _goodsDatabase;
        private readonly IAuctionsClient _auctionsClient;

        public GoodsRepository(IGoodsDatabase goodsDatabase, IAuctionsClient auctionsClient,
            IGoodsOnAuctionsSender rmqSender)
        {
            _goodsDatabase = goodsDatabase;
            _auctionsClient = auctionsClient;
            _rmqSender = rmqSender;
        }

        public async Task<Good> InsertGoodAsync(int ownerId, Good good)
        {
            if (good.Price <= 0)
            {
                throw new JsonValidationException(new[]
                {
                    new ValidationFailure("price",
                        "Price can't be zero or lower.")
                });
            }

            var auction = await PreProcessNewGood(good, ownerId);

            //TODO: min_seller_reputation
            var result = await _goodsDatabase.InsertGoodAsync(good, auction?.MaxGoods);
            if (result == null)
            {
                throw new FinalException(403, "Max goods are already posted on the auction.");
            }

            return result;
        }

        public Task<Good> GetUserGoodAsync(int ownerId, Guid goodId)
        {
            if (goodId == Guid.Empty)
            {
                throw new JsonValidationException(new[]
                    {new ValidationFailure("goodId", "Value must be non-default.")});
            }

            return _goodsDatabase.GetUserGoodAsync(ownerId, goodId);
        }

        public Task<bool> DeleteGoodAsync(int ownerId, Guid goodId)
        {
            if (goodId == Guid.Empty)
            {
                throw new JsonValidationException(new[]
                    {new ValidationFailure("goodId", "Value must be non-default.")});
            }

            return _goodsDatabase.DeleteGoodAsync(ownerId, goodId);
        }

        public async Task<bool> RemoveGoodAsync(int auctionOwnerId, Guid goodId)
        {
            if (goodId == Guid.Empty)
            {
                throw new JsonValidationException(new[]
                    {new ValidationFailure("goodId", "Value must be non-default.")});
            }

            var good = await _goodsDatabase.GetGoodByIdInternalAsync(goodId);
            if (!good.AuctionId.HasValue)
            {
                throw new FinalException(400, "Good is not staged on any auction.");
            }

            var auction = await _auctionsClient.GetUserAuctionByIdAsync(auctionOwnerId, good.AuctionId.Value);
            if (auction == null)
            {
                throw new FinalException(403, "Can't remove good from foreign auction.");
            }

            return await _goodsDatabase.RemoveGoodAsync(goodId);
        }

        public async Task<Good> EditUserGoodAsync(int userId, Good good, Guid goodId)
        {
            if (goodId == Guid.Empty)
            {
                throw new JsonValidationException(new[]
                    {new ValidationFailure("goodId", "Value must be non-default.")});
            }

            if (good.Price <= 0)
            {
                throw new JsonValidationException(new[]
                {
                    new ValidationFailure("price",
                        "Price can't be zero or lower.")
                });
            }

            var oldGood = await _goodsDatabase.GetUserGoodAsync(userId, goodId);
            if (oldGood == null)
            {
                throw new FinalException(404, "User has no such good to edit.");
            }

            if (oldGood.AuctionId.HasValue)
            {
                await CheckAuctionBoundaries(oldGood.AuctionId.Value, userId);
            }

            var userAuction = await PreProcessGood(good, userId);

            //TODO: min_seller_reputation
            var result = await _goodsDatabase.EditGoodAsync(good, userAuction?.MaxGoods);
            if (result == null)
            {
                throw new FinalException(403, "Max goods are already posted on the auction.");
            }

            if (result.AuctionId.HasValue)
            {
                _rmqSender.SendInfoAboutNewGoodOnAuction(result, userAuction);
            }

            return result;
        }

        public Task<List<Good>> GetAllGoodsAsync(int ownerId)
        {
            return _goodsDatabase.GetAllGoodsAsync(ownerId);
        }

        public Task<List<Good>> GetGoodsOnAuctionInternalAsync(Guid auctionId)
        {
            if (auctionId == Guid.Empty)
            {
                throw new JsonValidationException(new[]
                    {new ValidationFailure("auctionId", "Value must be non-default.")});
            }

            return _goodsDatabase.GetGoodsOnAuctionInternalAsync(auctionId);
        }

        private async Task<Auction> CheckAuctionBoundaries(Guid auctionId, int userId)
        {
            var userAuction = await _auctionsClient.GetUserAuctionByIdAsync(userId, auctionId);
            if (userAuction == null)
            {
                throw new FinalException(404, "User has no such auction.");
            }

            if (userAuction.EndTime.HasValue || userAuction.StartTime <= DateTime.Now.AddMinutes(30))
            {
                throw new FinalException(403, "Auction is in frozen mode now.");
            }

            //TODO: min_seller_reputation
            return userAuction;
        }

        private async Task<Auction> PreProcessNewGood(Good good, int ownerId)
        {
            var goodAuction = await PreProcessGood(good, ownerId);
            good.PreviousOwnerId = null;
            good.LastTransferDate = null;
            return goodAuction;
        }

        private async Task<Auction> PreProcessGood(Good good, int ownerId)
        {
            Auction auction = null;
            good.CurrentOwnerId = ownerId;
            if (good.AuctionId != null)
            {
                auction = await CheckAuctionBoundaries(good.AuctionId.Value, ownerId);
            }

            return auction;
        }
    }
}